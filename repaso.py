"""
#Python es un lenguaje No tipado, es decir, que no es necesario indicar de que tipo de variable se trata
numero_entero = 10
print(numero_entero)
print(type(numero_entero)) #imprime a que tipo de dato pertenece
numero_flotante = 10.5
print(numero_flotante)
print(type(numero_flotante))
variable_boleana=False
print(variable_boleana)
print(type(variable_boleana))
variable_string = "Hola a todos"
print(variable_string)
print(type(variable_string))

#tipos de datos
### LISTAS  -> Contienen diferentes variables en una sola variable
variable_lista = [1,"hola",False,3.5]
print("La variable lista es= ",variable_lista)

## TUPLAS  -> Una lista que no se puede modificar luego
variable_tupla = (1,"hola")
print("La variable tupla contiene: ",variable_tupla)
# aca se accede a la posicion de una lista
print("El primer elemento de la lista es: ", variable_lista[0])

## DICCIONARIO  -> {Palabra clave: definicion}
#                  {"rosa":flor del rosal""}

variable_diccionario={"uno":1,"dos":"Hola","tres":False,"cuatro":3.5}
print("El diccionario: ", variable_diccionario)
print("El contenido de la llave es: ", variable_diccionario["uno"])


##Tranformaciones de datos -> casteo de datos

#print("La lista convertida en cadena es: ",variable_string.join(variable_lista))
"""

"""
#INPUT AND PRINT
x = int(input("Ingrese un número: "))
print("El numero ingresado fue: ",x)

"""
"""
#Escribir un programa que muestre por pantalla la cadena ¡Hola Mundo!
cadena="¡Hola Mundo!"
print(cadena)
"""

"""
#Escribir un programa que almacene la cadena ¡Hola Mundo! en una variable y luego muestre por pantalla el contenido de la variable
cadena=str(input("Ingrese la frase ¡Hola Mundo! "))
print(cadena)
"""
"""
# Escribir un programa que pregunte el nombre del usuario en la consola y despues de que el usuario lo introduzca muestre por pantalla
# la cadena ¡Hola <nombre>! donde <nombre> es el nombre que el usuario haya introducido

nombre=str(input("Ingrese su nombre: "))
print(f"¡Hola {nombre}!")
print("¡Hola "+nombre+"!")
"""

"""
#CREAR UNA LIBRETA DE CONTACTOS CON LOS NOMBRE DE LOS COMPAÑEROS UTILIZANDO UN DICCIONARIO
libreta={}
nContactos = int(input("Ingrese la cantidad de contactos: "))
for clave in range(0,nContactos):
    nombre = str(input("Ingrese el nombre: "))
    libreta[clave]=nombre

print("La libreta contiene los siguientes nombres: ",libreta.values())
"""


"""
#if - else (bifurcaciones)
if(3>3 and 3>2):
    if(3>=3):
        print("La condicion se cumplio")
    else:
        print("No se cumplio")
        if(10>5):
            print("Pero esta si se cumplio")
else:
    print("La condición no se cumplió")
"""

"""
#ciclo while
numero=1
while(numero<20):
    print("El número tiene valor de: ",numero)
    numero+=1
    if(numero>5 and numero<8):
        print("El número puede ser seis o siete")
        while(numero<11):
            print("El número dentro del while2 es: ",numero)
            numero+=1
print("Al final el número tiene el valor de: ",numero)
"""


"""
#CICLO FOR
#RECORRER UNA LISTA
lista = [1,"dos","tres",4.5,False,[1,2,3],{"uno":"Enero","dos":"Febrero"}]
for elemento in lista:
    print(elemento)
print("================================")
for i in range (0,10): #i va ir desde cero hasta 9
    print(i)
print("================================")
for i in(range(0,len(lista))):
    elemento =lista[i]
    print("El elemento en la posición ",i," de la lista es; ",elemento)
"""

"""
#SENTENCIAS BREAK AND CONTINUE1
contador=0
while(contador<10):
    print(contador)
    contador+=1
    if(contador==7):
        continue
    if(contador==9):
        break
    contador+=1
"""

"""
#FUNCIONES
import funciones

lista_numeros = [5,5,3,1,5]
media = funciones.calcular_media(lista_numeros)
print(media)


"""
try:
    numero = int(input("Ingrese un número: "))
    print("El número ingresado es: ",numero)
except:
    print("El formato ingresado no es valido")
print("Aca hay un codigo importante para ser ejecutado")